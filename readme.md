####  PLAsTiCC supernova classification - CNN approach

- [notebook preview](https://micstan.gitlab.io/share_zone/lsst_cnn.html)
- In this notebook i try to classify supernova stars based on photometry recordings as shared with [PLAsTiCC dataset](https://plasticc.org) released together with this [kaggle competition](https://www.kaggle.com/c/PLAsTiCC-2018).
- The dataset has been syntheticaly created in order to help astronomers prepare for new [Large Synoptic Survey Telescope](https://lsst-tvssc.github.io/) which is about to revolutionazie the field.
- Most of the kernels shared on Kaggle ([including the winner](https://www.kaggle.com/c/PLAsTiCC-2018/discussion/75033))
used some type of feature extraction process (e.g. modeling with gausian processes) and then ensemble models like LightGBM and different blends (like [here](https://www.kaggle.com/c/PLAsTiCC-2018/discussion/75012) or [here](https://www.kaggle.com/c/PLAsTiCC-2018/discussion/75050)). There were some trials on raw data modeling mostly using some form of RNNs.
- In this notebook i want to try an **alternative solution: single functional Keras model combining raw flux inputs (light curve magnitudes on 6 passbands) with metadata and some basic extracted features. Raw flux inputs are reshaped into an array with errors as channels and 6 different passbands as the width of the array. As a results 2D CNN can be used.**
- There are two key ingredients that seems to be crucial for high score on this dataset: 1. Data augmentation (original test set is very different from the training set, general quality of measurement is worst so some form of degradation is needed) 2. Predicting "Unknown" objects - class 99 for wich we do not have labeled training set. Both problems have been ommited in this notebook.
